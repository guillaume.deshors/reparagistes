+++
author = "Guillaume"
+++

## C'est où ??

Ça se passe dans les locaux temporaires de l'Atelier Tiers Lieu au [240 rue de l'Industrie, ZAC de Chavanis, 69550 Saint-Jean la Bussière](https://www.openstreetmap.org/?mlat=45.98025&mlon=4.32588#map=17/45.98025/4.32588).

<iframe width="425" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=4.316843748092652%2C45.97705031283432%2C4.334760904312135%2C45.983357701210586&amp;layer=mapnik&amp;marker=45.98020409683749%2C4.325802326202393" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=45.98020&amp;mlon=4.32580#map=17/45.98020/4.32580">Afficher une carte plus grande</a></small>

## C'est quand ??

Les sessions ont lieu tous les **deuxième samedi du mois**, de 9h30 à 11h30, 

## C'est combien ??

C'est gratuit&nbsp;! La seule condition est d'être adhérent de l'Atelier, à partir de 2€. Les adhérents de la MJC d'Amplepuis sont aussi les bienvenus, grâce à un partenariat.

## Il faut réserver ??

Ce n'est pas obligatoires. Mais si vous nous contactez à l'avance, on pourra vous confirmer s'il y a bien un créneau pour vous, et si vous nous avez fourni le plus de détails possible sur l'appareil en question et sa panne, ça peut nous aider à venir avec les outils ou le matériel nécessaire.

## C'est garanti ??

Nous sommes une équipe de bénévoles passionnés qui faisons ça pour le plaisir de partager un bon moment autour de discussions techniques, pour rendre service, et pour des raisons écologiques. Aucun d'entre nous n'est professionnel de la réparation, et l'atelier n'est pas payant. Nous mettons tout en oeuvre pour ne pas ajouter une panne à un appareil, mais en dernier recours la personne qui apporte son appareil en reste responsable.