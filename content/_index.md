+++
author = "Guillaume"
+++

## Ici, on répare, on agit&nbsp;!

*Les Réparagistes*, c’est un groupe de bénévoles qui a décidé de passer à l’action pour donner une seconde vie à nos objets. Bénévoles à l’esprit solidaire nourris d’éducation populaire, nous travaillons toujours dans une ambiance conviviale. Venez au [repair café](https://www.repaircafe.org/fr/) avec votre objet cassé et nous essaierons de le réparer ensemble&nbsp;!

_Attention, changement de date exceptionnel en janvier : la session n'aura pas lieu le 11 mais le **18 janvier 2025** !_

![](/images/photos/ressort-aspi-6mains.jpg)

Nous faisons partie de [l'Atelier Tiers-Lieu](https://ateliertierslieu.org/) à Amplepuis (69550).

Les sessions ont lieu tous les **deuxième samedi du mois**, de 9h30 à 11h30, dans les locaux temporaires de l'Atelier au [240 rue de l'Industrie, ZAC de Chavanis, 69550 Saint-Jean la Bussière](https://www.openstreetmap.org/?mlat=45.98025&mlon=4.32588#map=17/45.98025/4.32588).

Prochaines dates : 18 janvier, 8 février, 8 mars, 12 avril, 10 mai, 14 juin