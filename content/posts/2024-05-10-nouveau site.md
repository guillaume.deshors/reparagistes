+++
author = "Guillaume"
title = "Un nouveau site pour les réparagistes"
date = "2024-05-10"
description = "pour une meilleure communication"
tags = [
    "communication",
]
+++

Après 3 ans de fonctionnement, nous faisons le constat d'un besoin d'un nouveau support de communication. Ce nouveau site nous permettra de poster les dates et les nouvelles qui concernent l'atelier de réparation.

N'hésitez pas à revenir le consulter, il est tout frais et devrait s'étoffer un peu.